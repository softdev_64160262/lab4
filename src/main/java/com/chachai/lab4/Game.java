/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chachai.lab4;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Game {

    Player player1 = new Player("X", 0, 0, 0);
    Player player2 = new Player("O", 0, 0, 0);
    private Board board = new Board(player1, player2);
    boolean isFinish = false;
    String[][] b = board.getBoard();
    Scanner kb = new Scanner(System.in);

    public void startGame() {
        System.out.println("Welcome to OX game!!!");
        System.out.print("Start Game? (y/n): ");
        String start = kb.nextLine().toLowerCase();
        while (!start.equals("n") && !start.equals("y")) {
            System.out.print("Start Game? (y/n): ");
            start = kb.nextLine().toLowerCase();
        }
        isFinish = start.equals("n");
    }

    public void showBoard() {
        String[][] b = board.getBoard();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void showTurn() {
        System.out.println("Turn --> " + board.getCurrentPlayer().getSymbol());
    }

    private void changeTurn() {
        board.nextTurn();
    }

    private boolean inputRowCol() {
        System.out.print("Input row (1-3): ");
        int row = kb.nextInt();
        System.out.print("Input column (1-3): ");
        int col = kb.nextInt();
        return board.setRowCol(row, col);
    }

    
    public void play() {
        startGame();
        while (!isFinish) {
            b = board.getBoard();
            showBoard();
            showTurn();
            if (inputRowCol()) {
                if (board.checkWin()) {
                    showBoard();
                    showWinner();
                    showTurn();
                    showPoint();
                    playAgain();
                    board.resetBoard();
                    b = board.getBoard();
                } if(board.checkDraw()){
                    showBoard();
                    showDraw();
                    showTurn();
                    showPoint();
                    playAgain();
                    board.resetBoard();
                    b = board.getBoard();
                }
                board.nextTurn();
            }
        }
        showGoodBye();
    }

    public void playAgain() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Do you want to play again? (y/n): ");
        String again = kb.next().toLowerCase();
        while (!again.toLowerCase().equals("n") && !again.toLowerCase().equals("y")) {
            System.out.print("Invalid input. Please enter 'y' or 'n': ");
            again = kb.next().toLowerCase();
        }
        if (again.equals("n")) {
            isFinish = true;
            System.out.println("    ----- Have a nice day ----- ");

        }

    }

    private void showPoint() {
        System.out.println(player1.toString());
        System.out.println(player2.toString());
    }

    private void showGoodBye() {
        System.out.println("Good Bye!!");
        showPoint();
    }
    
    private void showWinner(){
    System.out.println("The winner is " + board.getCurrentPlayer().getSymbol());
    }
    
    private void showDraw(){
       System.out.println("  ----- Draw -----  ");  
    }

    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}
