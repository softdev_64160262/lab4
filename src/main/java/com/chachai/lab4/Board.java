/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chachai.lab4;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Board {

    private String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
    private Player player1, player2, currentPlayer;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Board(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public void setBoard(String[][] board) {
        this.board = board;
    }

    public String[][] getBoard() {
        return board;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        Scanner kb = new Scanner(System.in);
        if ((row > 0 && row < 4) && (col > 0 && col < 4)) {
            if (board[row - 1][col - 1] == "_") {
                board[row - 1][col - 1] = currentPlayer.getSymbol();
                this.col = col;
                this.row = row;
                return true;
            }
            return false;
        } else {
            return false;

        }

    }

    public void nextTurn() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public boolean checkRows() {
        for (int j = 0; j < 3; j++) {
            if (!board[row - 1][j].equals(currentPlayer.getSymbol())) {
                return false;
            }
        }
        return true;
    }

    public boolean checkColums() {
        for (int j = 0; j < 3; j++) {
            if (!board[col - 1][j].equals(currentPlayer.getSymbol())) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal() {
        if (row - 1 == col - 1) {
            for (int i = 0; i < 3; i++) {
                if (!board[i][i].equals(currentPlayer.getSymbol())) {
                    return false;
                }

            }
            return true;
        }
        if ((row + col) - 2 == 2) {
            for (int i = 0; i < 3; i++) {
                if (!board[i][2 - i].equals(currentPlayer.getSymbol())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean checkDraw() {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j].equals("_")) {
                    return false;
                }

            }
        }
        return true;
    }

    public boolean checkWin() {
        if (checkRows() || checkColums() || checkDiagonal()) {
            saveWin("win");
            return true;
        } else {
            if (checkDraw()) {
                saveWin("draw");
                return true;
            }
        }
        return false;
    }

    public void saveWin(String type) {
        if (type.equals("win")) {
            if (currentPlayer == player1) {
                currentPlayer.win();
                player2.lose();
            } else {
                player2.win();
                player1.lose();
            }
        }
        if (type.equals("draw")) {
            if (currentPlayer == player1) {
                currentPlayer.draw();
                player2.draw();
            }
        }
    }
    
    public void resetBoard(){
    String[][] boardnew = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
        setBoard(boardnew);
    }

}
